import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import LoginValidator from 'App/Validators/Auth/LoginValidator'

export default class AuthController {
    public async login ({ response, auth  } : HttpContextContract) {
        const {user:{email,password},} = await request.validate(LoginValidator)
        const token = await auth.attempt(email, password)
        const user = auth.user!
        return response.ok({
            user: auth.user
        })
    }
    public async me ({ request, response, auth  } : HttpContextContract) {
        const {user:{email,password},} = await request.validate(LoginValidator)
        const token = await auth.attempt(email, password)
        const user = auth.user!
        return response.ok({
            user: {
                token: token.token,
                ...user.serialize(),
            },
        })
    }
}
